In order to rename the file, use "git mv <current_name> <new_name>"
Note: Upon doing this, the file must have been already staged (kept track of) by Git

git restore --staged <file> command is used to remove the file from the staging area

If the renaming procedure is performed using Git bash command instead of commands that invokes Git itself,
when you run "git status", Git interprets this action as two separate operations:
+ We deleted the old file (the file that was renamed)
+ We added a new file (the file whose new name was given)
=> Thus, use "git add -A" command to recursively (optional for directory) add all files that were renamed to the staging area

git add -u: update the Git index