When editing file, you may choose to add files to the staging area so that Git is able to track the files.
If more changes are performed on the existing files, there will be two version of the file present on both
the working directory, and the staging area. 

git restore --unstaged <file> (unstage a file from the staging area)
git restore <file> (discard the changes on the current file, typically this is prior to pushing this change to update the version
                    on the staging area)

