#ifndef TASK_STRUCT_H
#define TASK_STRUCT_H_

//Block structure used for scheduling each tasks
typedef struct TaskControlBlock
{
   void (*myTask)(void);
   void *taskDataPtr;
   struct TaskControlBlock *next;
   struct TaskControlBlock *prev;
} TCB;

#endif