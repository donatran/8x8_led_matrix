#ifndef QUEUE_H_
#define QUEUE_H_

typedef struct Queue
{
   void (*myTask)(void *);
   void *taskDataPtr;

} QueueData

#endif //QUEUE_H_