uint32_t clear_pin = 4; //Clear pin of the shift register (active-low SRCLR pin)
uint32_t shift_reg_clk_pin = 5; //shift register clock pin (active-high SRCLK)
uint32_t storage_reg_clk_pin = 6; //storage register clock pin (active-high RCLK)(latch clock in the demo)
uint32_t serial_data_pin = 7; //Serial input pin of the shift register (active-high SER pin)


#define _8_BIT_SHIFT 8


void setup() {
  pinMode(clear_pin, OUTPUT);
  pinMode(shift_reg_clk_pin, OUTPUT);
  pinMode(storage_reg_clk_pin, OUTPUT);
  pinMode(serial_data_pin, OUTPUT);

  //Reset the LED
  digitalWrite(clear_pin, LOW);  //Clear all the shift registers parallel output pins.
  digitalWrite(clear_pin, HIGH); //Disable the clear pin so that the shift register can function
}

void shiftOutVer(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val)
{
      uint8_t i;

      for (i = 0; i < 8; i++)  {
            if (bitOrder == LSBFIRST)
                  digitalWrite(dataPin, !!(val & (1 << i)));
            else      
                  digitalWrite(dataPin, !!(val & (1 << (7 - i))));
                  
            digitalWrite(clockPin, HIGH);
            digitalWrite(clockPin, LOW);            
      }
}

void loop() {

  for (uint32_t cntr = 0; cntr < (1 << _8_BIT_SHIFT); ++cntr)
  {
    digitalWrite(storage_reg_clk_pin, LOW); //Prior to moving the bits to the storage register, 
                                            //disable the latch clock so that no shift could occur
                                            //during out setup

    shiftOutVer(serial_data_pin, shift_reg_clk_pin, MSBFIRST, cntr); //setup
    digitalWrite(storage_reg_clk_pin, HIGH); //move the value from shift register to storage register and output to the LED
    delay(500); //delay is important to see the transition (real-time embedded operation requires lots of delay)
  }
}
